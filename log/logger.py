def libcs_set_logger():
    from logging import (getLogger, StreamHandler, FileHandler, Formatter,
                         DEBUG, INFO, WARNING, ERROR)

    LOGLEVEL = 'DEBUG'

    # logger設定
    logger = getLogger(__name__)
    logger.setLevel(LOGLEVEL)

    # coloedlogs設定
    coloredlogs.CAN_USE_BOLD_FONT = True
    coloredlogs.DEFAULT_FIELD_STYLES = {'asctime': {'color': 'green'},
                                        'hostname': {'color': 'magenta'},
                                        'levelname': {'color': 'cyan', 'bold': True},
                                        'name': {'color': 'blue'},
                                        'programname': {'color': 'cyan'}
                                        }
    coloredlogs.DEFAULT_LEVEL_STYLES = {'critical': {'color': 'red', 'bold': True},
                                        'error': {'color': 'red'},
                                        'warning': {'color': 'yellow'},
                                        'notice': {'color': 'magenta'},
                                        'info': {},
                                        'debug': {'color': 'green'},
                                        'spam': {'color': 'green', 'faint': True},
                                        'success': {'color': 'green', 'bold': True},
                                        'verbose': {'color': 'blue'}
                                        }

    coloredlogs.install(level='INFO', logger=logger, fmt='%(asctime)s.%(msecs)-3d' +
                        ' [%(levelname)-4s]:' +
                        ' %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    # handler設定
    # フォーマット設定
    handler_format = Formatter('%(asctime)s.%(msecs)-3d' +
                               ' [%(levelname)-4s]:' +
                               ' %(message)s',
                               datefmt='%Y-%m-%d %H:%M:%S')
    # 標準出力
    stream_handler = StreamHandler()
    stream_handler.setLevel(LOGLEVEL)
    stream_handler.setFormatter(handler_format)

    # loggerにhandlerセット
    # logger.addHandler(stream_handler)

    # ファイル出力
    file_handler = FileHandler(LOG_SYSTEM_FILE_PATH, 'a', encoding='utf-8')
    file_handler.setLevel(LOGLEVEL)
    file_handler.setFormatter(handler_format)
    logger.addHandler(file_handler)

    return logger


logger = libcs_set_logger()
