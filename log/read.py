import os
import matplotlib.pyplot as plt
import pandas as pd
import sys

colors = [
    'red',
    'blue',
    'black',
    'rosybrown',
    'olive',
    'lime',
    'gray',
    'saddlebrown',
    'darkorange',
    'yellow',
    'darkgreen',
    'cyan',
    'darkviolet',
    'megenta',
    'deeppink',
    'firebrick',
    'peachpuff',
    'goldenrod',
    'darkslategrey',
    'slateblue',
    'lightpink']


def save_3d_route(x, y, z, file_name):
    fig = plt.figure(figsize=(12, 9))
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlabel('$latitude$', size=14)
    ax.set_ylabel('$longitude$', size=14)
    ax.set_zlabel('$altitude$', size=14)
    ax.set_title('3d route of drone')
    ax.plot(x, y, z, color='red', label='route')
    plt.legend(loc='upper left', fontsize=12)
    plt.savefig(file_name)
    plt.show()


def save_multiple_graph(x, x_label, y_list, y_labels, index, column, file_name, motor_times=[]):
    fig = plt.figure(figsize=(6.4 * column, 4.8 * index))
    ax = []
    for i in range(int(index * column)):
        ax.append(fig.add_subplot(index, column, i + 1))
    for i in range(int(index * column)):
        ax[i].plot(x, y_list[i], color=colors[i % len(colors)], label=y_labels[i])
        if len(motor_times) != 0:
            for motor_time in motor_times:
                ax[i].axvline(motor_time, color="black", linewidth=1.5)
        ax[i].legend()
    fig.tight_layout()
    plt.savefig(file_name)
    plt.show()


def save_single_graph(x, x_label, y, y_label, file_name, color=colors[0], motor_times=[]):
    plt.figure()
    # plt.rcParams['font.size'] = 18
    plt.title(y_label)
    plt.ylabel(y_label)
    plt.xlabel(x_label)

    plt.plot(x, y, color, linewidth=2, label=y_label)
    plt.legend()
    plt.grid()
    if len(motor_times) != 0:
        for motor_time in motor_times:
            plt.axvline(motor_time, c="black", linewidth=1.5)
    plt.tight_layout()
    plt.savefig(file_name)
    plt.show()


def save_double_plot_graph(x, x_label, y_list, y_labels, file_name, motor_times=[]):
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.plot(x, y_list[0], colors[0], linewidth=2, label=y_labels[0])
    ax2 = ax1.twinx()
    ax2.plot(x, y_list[1], colors[1], linewidth=2, label=y_labels[1])
    h1, l1 = ax1.get_legend_handles_labels()
    h2, l2 = ax2.get_legend_handles_labels()
    ax1.legend(h1 + h2, l1 + l2, loc='lower right')
    ax1.set_xlabel(x_label)
    ax1.set_ylabel(y_labels[0])
    ax1.grid(True)
    if len(motor_times) != 0:
        for motor_time in motor_times:
            ax1.axvline(motor_time, c="black", linewidth=1.5)
    ax2.set_ylabel(y_labels[1])
    fig.savefig(file_name)
    plt.show()


def plot(file_name, x_column, num_graph, y_columns, motor_times=[]):
    if not os.path.isdir(file_name[:-4]):
        os.makedirs(file_name[:-4])

    df = pd.read_csv(file_name)
    x = [float(i) for i in list(df.iloc[:, x_column])]
    x_label = df.columns[0]

    if num_graph == 0:
        for i in range(len(df.columns) - 1):
            y = [float(j) for j in list(df.iloc[:, i + 1])]
            y_label = df.columns[i + 1]
            save_single_graph(x,
                              x_label,
                              y,
                              y_label,
                              file_name[:-4] + "/" + file_name.split("/")[-1][:-4] + "_" + y_label + '.png',
                              color=colors[i % len(colors)],
                              motor_times=motor_times)
    elif num_graph == 100:
        y_list = []
        y_labels = []
        for i in range(3):
            y_list.append([float(j) for j in list(df.iloc[:, y_columns[i]])])
            y_labels.append(df.columns[y_columns[i]])
        save_3d_route(y_list[0], y_list[1], y_list[2], file_name[:-4] + "/" +
                      file_name.split("/")[-1][:-4] + "_route" + '.png')
    elif num_graph == 1 and len(y_columns) == 1:
        y = [float(j) for j in list(df.iloc[:, y_columns[0]])]
        y_label = df.columns[y_columns[0]]
        save_single_graph(x,
                          x_label,
                          y,
                          y_label,
                          file_name[:-4] + "/" + file_name.split("/")[-1][:-4] + "_" + y_label + '.png',
                          color=colors[0],
                          motor_times=motor_times)
    elif num_graph == 1 and len(y_columns) == 2:
        y_list = []
        y_labels = []
        for i in range(2):
            y_list.append([float(j) for j in list(df.iloc[:, y_columns[i]])])
            y_labels.append(df.columns[y_columns[i]])
        tmp = "_".join(y_labels)
        save_double_plot_graph(x,
                               x_label,
                               y_list,
                               y_labels,
                               file_name[:-4] + "/" + file_name.split("/")[-1][:-4] + "_" + tmp + '.png',
                               motor_times=motor_times)
    elif num_graph > 1 and len(y_columns) == num_graph:
        y_list = []
        y_labels = []
        for i in range(num_graph):
            y_list.append([float(j) for j in list(df.iloc[:, y_columns[i]])])
            y_labels.append(df.columns[y_columns[i]])
        tmp = "_".join(y_labels)
        if num_graph % 2 == 0:
            save_multiple_graph(x,
                                x_label,
                                y_list,
                                y_labels,
                                num_graph / 2,
                                2,
                                file_name[:-4] + "/" + file_name.split("/")[-1][:-4] + "_" + tmp + '.png',
                                motor_times=motor_times)
        elif num_graph % 3 == 0:
            save_multiple_graph(x,
                                x_label,
                                y_list,
                                y_labels,
                                num_graph / 3,
                                3,
                                file_name[:-4] + "/" + file_name.split("/")[-1][:-4] + "_" + tmp + '.png',
                                motor_times=motor_times)
        else:
            save_multiple_graph(x,
                                x_label,
                                y_list,
                                y_labels,
                                1,
                                num_graph,
                                file_name[:-4] + "/" + file_name.split("/")[-1][:-4] + "_" + tmp + '.png',
                                motor_times=motor_times)
    else:
        print("invalid arg")


if __name__ == "__main__":
    args = sys.argv
    file_name = args[1]  # ファイル名

    x_column = 0
    num_graph = 0
    y_columns = []

    if len(args) > 2:
        x_column = int(args[2])
        num_graph = int(args[3])
        if len(args) > 4:
            for i in range(len(args) - 4):
                y_columns.append(int(args[i + 4]))

    motor_times = []
    if "motor" in file_name:
        with open(file_name, "r") as f:
            motor_times = [float(s.strip()) for s in f.readlines()]
        file_name = file_name.replace("_motor", "")

    plot(file_name, x_column, num_graph, y_columns, motor_times)
