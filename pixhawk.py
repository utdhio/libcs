def check(func):
    async def wrapper(*args, **kwargs):
        global IS_RUNNING
        if IS_RUNNING:
            try:
                await func(*args, **kwargs)
            except Exception as e:
                logger.error(e)
        else:
            return
    return wrapper


def tryasync(func):
    async def wrapper(*args, **kwargs):
        try:
            await func(*args, **kwargs)
        finally:
            await asyncio.sleep(0.1)
    return wrapper


class LibcsDetection:
    def __init__(self):
        self.frame = 0
        self.lat = 0
        self.lng = 0
        self.pitch = 0
        self.roll = 0
        self.yaw = 0
        self.z = 0

    def update(self, frame, lat, lng, pitch, roll, yaw, z):
        self.frame = frame
        self.lat = lat
        self.lng = lng
        self.pitch = np.deg2rad(pitch)
        self.roll = np.deg2rad(roll)
        self.yaw = np.deg2rad(yaw)
        self.z = z


class LibcsPixhawk:
    def __init__(self, system_address=SYSTEM_ADDRESS_DEFAULT):
        self.system_address = system_address
        if IS_SIM:
            self.system_address = SYSTEM_ADDRESS_SIM
        # odometry
        self.x_m = 0.0
        self.y_m = 0.0
        self.z_m = 0.0
        # global position
        self.latitude_deg = 0.0
        self.longitude_deg = 0.0
        self.absolute_altitude_m = 0.0
        self.relative_altitude_m = 0.0
        # home positioin
        self.home_latitude_deg = 0.0
        self.home_longitude_deg = 0.0
        self.home_absolute_altitude_m = 0.0
        self.home_relative_altitude_m = 0.0

        # ned velocity
        self.north_m_s = 0.0
        self.east_m_s = 0.0
        self.down_m_s = 0.0
        # odometryvelocity
        self.x_m_s = 0.0
        self.y_m_s = 0.0
        self.z_m_s = 0.0
        # gps
        self.x_m_gps = 0.0
        self.y_m_gps = 0.0

        # angle
        self.pitch_deg = 0.0
        self.roll_deg = 0.0
        self.yaw_deg = 0.0

        # angular velocity
        self.pitch_rad_s = 0.0
        self.roll_rad_s = 0.0
        self.yaw_rad_s = 0.0
        # odometry angular velocity
        self.odometry_roll_rad_s = 0.0
        self.odometry_pitch_rad_s = 0.0
        self.odometry_yaw_rad_s = 0.0

        # lidar
        self.lidar = -1

        # control
        self.control_roll = 0.0
        self.control_pitch = 0.0
        self.control_yaw = 0.0

        # arm
        self.armed = False
        # in_air
        self.in_air = False
        # flight mode
        self.flight_mode = 0.0
        # rc
        self.rc_is_available = False
        self.rc_signal_strength_percent = 0.0
        # voltage
        self.voltage_v = 0.0
        self.remaining_percent = 0.0
        # gps
        self.num_satellites = 0.0
        self.fix_type = 0.0
        # check
        self.is_gyrometer_calibration_ok = 0.0
        self.is_accelerometer_calibration_ok = 0.0
        self.is_magnetometer_calibration_ok = 0.0
        self.is_level_calibration_ok = 0.0
        self.is_local_position_ok = 0.0
        self.is_global_position_ok = 0.0
        self.is_home_position_ok = 0.0
        self.is_health_all_ok = 0.0

        self.return_to_launch_altitude = 0.0
        self.return_to_launch_after_mission = False
        self.is_mission_finished = True
        self.mission_plan = ""
        self.mission_progress = ""

        # status_text
        self.status_text = ""

        # time
        self.time = 0.0

        # target
        self.lng_target = None
        self.lat_target = None
        self.alt_target = None
        self.yaw_target = None

        # detection
        self.detection = LibcsDetection()
        self.is_detect = False
        self.detection_interval = 0.5

    async def init(self):
        self.drone = mavsdk.System()
        self.is_detect = False
        await self.drone.connect(system_address=self.system_address)

        logger.info("Waiting for drone to connect...")
        async for state in self.drone.core.connection_state():
            if state.is_connected:
                logger.info(f"Drone discovered!")
                break

    @tryasync
    async def get_lidar(self):
        async for current_distance_m in self.drone.telemetry.distance_sensor():
            self.lidar = current_distance_m.current_distance_m / 100 * \
                np.cos(np.deg2rad(self.pitch_deg)) * np.cos(np.deg2rad(self.roll_deg))
            if self.lidar < 0:
                self.lidar = 0
            break

    @tryasync
    async def get_attitude_angle(self):
        async for angle in self.drone.telemetry.attitude_euler():
            self.pitch_deg = angle.pitch_deg
            self.roll_deg = angle.roll_deg
            self.yaw_deg = angle.yaw_deg
            break

    @tryasync
    async def get_velocity_ned(self):
        async for velocity_ned in self.drone.telemetry.velocity_ned():
            self.north_m_s = velocity_ned.north_m_s
            self.east_m_s = velocity_ned.east_m_s
            self.down_m_s = velocity_ned.down_m_s
            break

    @tryasync
    async def get_attitude_angular_vel(self):
        async for angular_vel in self.drone.telemetry.attitude_angular_velocity_body():
            self.pitch_rad_s = angular_vel.pitch_deg
            self.roll_rad_s = angular_vel.roll_deg
            self.yaw_rad_s = angular_vel.yaw_deg
            break

    @tryasync
    async def get_position(self):
        async for position in self.drone.telemetry.position():
            self.latitude_deg = position.latitude_deg
            self.longitude_deg = position.longitude_deg
            self.absolute_altitude_m = position.absolute_altitude_m
            self.relative_altitude_m = position.relative_altitude_m
            break

    @tryasync
    async def get_battery(self):
        async for battery in self.drone.telemetry.battery():
            self.voltage_v = battery.voltage_v
            self.remaining_percent = battery.remaining_percent
            break

    @tryasync
    async def get_gps_info(self):
        async for gps_info in self.drone.telemetry.gps_info():
            self.num_satellites = gps_info.num_satellites
            self.fix_type = gps_info.fix_type
            break

    @tryasync
    async def get_flight_mode(self):
        async for flight_mode in self.drone.telemetry.flight_mode():
            self.flight_mode = flight_mode
            break

    @tryasync
    async def get_status_text(self):
        async for status_text in self.drone.telemetry.status_text():
            self.status_text = status_text
            break

    @tryasync
    async def get_health(self):
        async for health in self.drone.telemetry.health():
            self.is_gyrometer_calibration_ok = health.is_gyrometer_calibration_ok
            self.is_accelerometer_calibration_ok = health.is_accelerometer_calibration_ok
            self.is_magnetometer_calibration_ok = health.is_magnetometer_calibration_ok
            self.is_level_calibration_ok = health.is_level_calibration_ok
            self.is_local_position_ok = health.is_local_position_ok
            self.is_global_position_ok = health.is_global_position_ok
            self.is_home_position_ok = health.is_home_position_ok
            break

    @tryasync
    async def get_actuator_control_target(self):
        async for actuator_control_target in self.drone.telemetry.actuator_control_target():
            self.control_roll = actuator_control_target.controls[0]
            self.control_pitch = actuator_control_target.controls[1]
            self.control_yaw = actuator_control_target.controls[2]
            break

    @tryasync
    async def get_home(self):
        async for home in self.drone.telemetry.home():
            self.home_latitude_deg = home.latitude_deg
            self.home_longitude_deg = home.longitude_deg
            self.home_absolute_altitude_m = home.absolute_altitude_m
            self.home_relative_altitude_m = home.relative_altitude_m
            break

    @tryasync
    async def get_landed_state(self):
        async for landed_state in self.drone.telemetry.landed_state():
            self.landed_state = landed_state
            break

    @tryasync
    async def get_armed(self):
        async for armed in self.drone.telemetry.armed():
            self.armed = armed
            break

    @tryasync
    async def get_rc_status(self):
        async for rc_status in self.drone.telemetry.rc_status():
            self.rc_is_available = rc_status.is_available
            self.rc_signal_strength_percent = rc_status.signal_strength_percent
            break

    @tryasync
    async def get_odometry(self):
        async for odometry in self.drone.telemetry.odometry():
            self.x_m = odometry.position_body.x_m
            self.y_m = odometry.position_body.y_m
            self.z_m = odometry.position_body.z_m
            self.x_m_s = odometry.velocity_body.x_m_s
            self.y_m_s = odometry.velocity_body.y_m_s
            self.z_m_s = odometry.velocity_body.z_m_s
            self.odometry_roll_rad_s = odometry.angular_velocity_body.roll_rad_s
            self.odometry_pitch_rad_s = odometry.angular_velocity_body.pitch_rad_s
            self.odometry_yaw_rad_s = odometry.angular_velocity_body.yaw_rad_s
            break

    @tryasync
    async def get_health_all_ok(self):
        async for health_all_ok in self.drone.telemetry.health_all_ok():
            self.is_health_all_ok = health_all_ok
            break

    @tryasync
    async def get_unix_epoch_time(self):
        async for unix_epoch_time in self.drone.telemetry.unix_epoch_time():
            self.time = unix_epoch_time
            break

    @tryasync
    async def get_in_air(self):
        async for in_air in self.drone.telemetry.in_air():
            self.in_air = in_air
            break

    @tryasync
    async def get_return_to_launch_after_mission(self):
        async for return_to_launch_after_mission in self.drone.mission.get_return_to_launch_after_mission():
            self.return_to_launch_after_mission = return_to_launch_after_mission
            break

    @check
    async def arm(self):
        if self.flight_mode == "TAKEOFF":
            logger.critical("Flight mode is TAKEOFF.")
            return
        logger.info("--Arming")
        await self.drone.action.arm()

    async def disarm(self):
        logger.info("--Disarming")
        await self.drone.action.disarm()

    async def hold(self):
        logger.info("--Holding")
        await self.drone.action.hold()

    @check
    async def takeoff(self):
        logger.info("--Taking off")
        await self.drone.action.takeoff()

    async def land(self):
        logger.info("--Landing")
        await self.drone.action.land()

    async def reboot(self):
        logger.info("--Reboot")
        await self.drone.action.reboot()

    async def shutdown(self):
        logger.info("--Shutdown")
        await self.drone.action.shutdown()

    async def kill(self):
        logger.info("--Kill")
        await self.drone.action.kill()

    async def return_to_launch(self):
        await self.drone.action.return_to_launch()

    async def offboard_start(self):
        logger.info("-- Starting offboard")
        try:
            await self.drone.offboard.start()
        except mavsdk.offboard.OffboardError as error:
            logger.info(f"Starting offboard mode failed with error code: {error._result.result}")
            await self.drone.action.disarm()
            return

    async def offboard_stop(self):
        try:
            await self.drone.offboard.stop()
        except mavsdk.offboard.OffboardError as error:
            logger.info(f"Stopping offboard mode failed with error code: \
                {error._result.result}")

    async def offboard_set_attitude(self, thrust=0.6, roll=0, pitch=0, yaw=0):
        await self.drone.offboard.set_attitude(mavsdk.offboard.Attitude(roll, pitch, yaw, thrust))

    async def offboard_set_position_ned(self, north_m=0, east_m=0, down_m=0, yaw_deg=0):
        await self.drone.offboard.set_position_ned(mavsdk.offboard.PositionNedYaw(north_m, east_m, down_m, yaw_deg))

    async def offboard_set_velocity_body(self, forward_m_s=0, right_m_s=0, down_m_s=0, yawspeed_deg_s=0):
        await self.drone.offboard.set_velocity_body(mavsdk.offboard.VelocityBodyYawspeed(forward_m_s, right_m_s, down_m_s, yawspeed_deg_s))

    async def offboard_set_velocity_ned(self, north_m_s=0, east_m_s=0, down_m_s=0, yaw_deg=0):
        await self.drone.offboard.set_velocity_ned(mavsdk.offboard.VelocityNedYaw(north_m_s, east_m_s, down_m_s, yaw_deg))

    @check
    async def goto_location(self, latitude_deg, longitude_deg, absolute_altitude_m, yaw_deg):
        logger.info(
            "--Go to location to lat: " +
            str(latitude_deg) +
            " lng:" +
            str(longitude_deg) +
            " alt:" +
            str(absolute_altitude_m) +
            " yaw:" +
            str(yaw_deg))
        await self.drone.action.goto_location(latitude_deg, longitude_deg, absolute_altitude_m, yaw_deg)

    async def get_takeoff_altitude(self):
        self.takeoff_altitude = await self.drone.action.get_takeoff_altitude()

    async def set_takeoff_altitude(self, takeoff_altitude):
        self.takeoff_altitude = takeoff_altitude
        await self.drone.action.set_takeoff_altitude(takeoff_altitude)

    async def get_maximum_speed(self):
        self.maximum_speed = await self.drone.action.get_maximum_speed()

    async def set_maximum_speed(self, maximum_speed):
        self.maximum_speed = maximum_speed
        await self.drone.action.set_maximum_speed(maximum_speed)

    async def get_return_to_launch_altitude(self):
        self.return_to_launch_altitude = await self.drone.action.get_return_to_launch_altitude()

    async def set_return_to_launch_altitude(self, return_to_launch_altitude):
        self.return_to_launch_altitude = return_to_launch_altitude
        await self.drone.action.set_return_to_launch_altitude(return_to_launch_altitude)

    async def send_status_text(self, msg):
        '''
        msg_type: messageの種類
          * 1はINFO
          * 他にもALERTとかERRORとかある
        '''
        msg_type = server_utility.StatusTextType(5)
        await self.drone.server_utility.send_status_text(msg_type, msg)

    async def cancel_mission_upload(self):
        logger.info("--Canceling Mission Upload")
        await self.drone.mission.cancel_mission_upload()
        logger.info("--Canceled Mission Upload")

    async def cancel_mission_download(self):
        logger.info("--Canceling Mission Download")
        await self.drone.mission.cancel_mission_download()
        logger.info("--Canceled Mission Download")

    async def clear_mission(self):
        logger.info("--Clearing Mission")
        await self.drone.mission.clear()
        logger.info("--Cleared Mission")

    async def downlad_mission(self):
        async for mission_plan in self.drone.mission.download_mission():
            logger.info("--Downloading Mission")
            self.mission_plan = mission_plan
            logger.info("--Downloaded Mission")
            break

    async def get_is_mission_finished(self):
        self.is_mission_finished = await self.drone.mission.is_mission_finished()

    async def set_return_to_launch_after_mission(self, enable):
        self.return_to_launch_after_mission = enable
        await self.drone.mission.set_return_to_launch_after_mission(enable)
        if enable:
            logger.info("return to launch enabled")
        else:
            logger.info("return to launch disabled")

    async def pause_mission(self):
        logger.info("--Pausing Mission")
        await self.drone.mission.pause_mission()
        logger.info("--Paused Mission")

    @tryasync
    async def get_mission_progress(self):
        async for mission_progress in self.drone.mission.mission_progress():
            self.mission_progress = mission_progress
            break

    async def set_current_mission_item(self, index):
        await self.drone.mission.set_current_mission_item(index)
        logger.info("set mission item index" + str(index))

    async def upload_mission(self, mission):
        logger.info("-- Uploading mission")
        await self.drone.mission.upload_mission(mission)
        logger.info("--Uploaded mission")

    async def upload_geofence(self, polygons):
        logger.info("Uploading geofence...")
        await self.drone.geofence.upload_geofence(polygons)
        logger.info("Geofence uploaded!")

    async def start_mission(self):
        logger.info("-- Starting mission")
        await self.drone.mission.start_mission()

    async def calibrate_gyro(self):
        logger.info("-- Starting gyroscope calibration")
        async for progress_data in self.drone.calibration.calibrate_gyro():
            logger.info(progress_data)
        logger.info("-- Gyroscope calibration finished")

    async def calibrate_accelerometer(self):
        logger.info("-- Starting accelerometer calibration")
        async for progress_data in self.drone.calibration.calibrate_accelerometer():
            logger.info(progress_data)
        logger.info("-- Accelerometer calibration finished")

    async def calibrate_magnetometer(self):
        logger.info("-- Starting magnetometer calibration")
        async for progress_data in self.drone.calibration.calibrate_magnetometer():
            logger.info(progress_data)
        logger.info("-- Magnetometer calibration finished")

    async def calibrate_level_horizon(self):
        logger.info("-- Starting board level horizon calibration")
        async for progress_data in self.drone.calibration.calibrate_level_horizon():
            logger.info(progress_data)
        logger.info("-- Board level calibration finished")

    async def download_logfile(self):
        logger.info("-- Entry logs")
        entries = await self.drone.log_files.get_entries()
        logger.info("-- Entried logs")
        entry = entries[-1]
        logger.info(entry)
        date_without_colon = entry.date.replace(":", "-")
        path = f"/home/dhio/log/flight_log/log-{date_without_colon}.ulg"
        logger.info(path)
        previous_progress = -1
        async for progress in self.drone.log_files.download_log_file(entry, path):
            new_progress = round(progress.progress * 100)
            if new_progress != previous_progress:
                sys.stdout.write(f"\r{new_progress} %")
                sys.stdout.flush()
                previous_progress = new_progress
        logger.info("-- Downloaded logfile")

    async def set_target(self, lat, lng, alt, yaw, goto=False):
        self.lat_target = lat
        self.lng_target = lng
        self.alt_target = alt
        self.yaw_target = yaw
        logger.info("set target lat: " + str(lat) + ", lng: " + str(lng) + ", alt: " + str(alt) + ", yaw: " + str(yaw))
        if goto:
            await self.goto_location(lat, lng, alt, yaw)
        else:
            await asyncio.sleep(0.01)
