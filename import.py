import time
from datetime import datetime
import os
import copy
import asyncio
import aioconsole
import subprocess
import coloredlogs
import numpy as np
import pandas as pd
import sys
import re
import aiorun

START_TIME = time.time()

LIBCS_ENV = os.getenv("LIBCS_ENV", "")
LIBCS_PATH = os.getcwd()

IS_SIM = True if "SIM" in LIBCS_ENV else False
IS_BT = True if "BT" in LIBCS_ENV else False
IS_SAVER = True if "SAVER" in LIBCS_ENV else False
IS_AIR = True if "AIR" in LIBCS_ENV else False
IS_CDS = True if "CDS" in LIBCS_ENV else False
IS_CAMERA = True if "CAMERA" in LIBCS_ENV else False
IS_MOTOR = True if "MOTOR" in LIBCS_ENV else False
IS_PIXHAWK = False if "NO_PIXHAWK" in LIBCS_ENV else True

TODAY = f'{datetime.now():%m%d}'
NOW = f'{datetime.now():%m%d_%H%M%S}'

LOG_DATA_PATH = os.path.join(LIBCS_PATH, 'log/data', TODAY)
LOG_DATA_FILE_PATH = os.path.join(LOG_DATA_PATH, NOW + ".csv")
LOG_SYSTEM_PATH = os.path.join(LIBCS_PATH, 'log/system', TODAY)
LOG_SYSTEM_FILE_PATH = os.path.join(LOG_SYSTEM_PATH, NOW + ".log")
LOG_VIDEO_PATH = os.path.join("/".join(LIBCS_PATH.split("/")[:-1]), "videos", TODAY)
LOG_PICTURE_PATH = os.path.join(LIBCS_PATH, 'log/picture', TODAY)

if not os.path.isdir(LOG_DATA_PATH):
    os.makedirs(LOG_DATA_PATH, exist_ok=True)
if not os.path.isdir(LOG_SYSTEM_PATH):
    os.makedirs(LOG_SYSTEM_PATH, exist_ok=True)
if IS_CAMERA:
    if not os.path.isdir(LOG_VIDEO_PATH):
        os.makedirs(LOG_VIDEO_PATH, exist_ok=True)
    if not os.path.isdir(LOG_PICTURE_PATH):
        os.makedirs(LOG_PICTURE_PATH, exist_ok=True)

save_dict = {}
main_coroutines = []
IS_RUNNING = False

try:
    # spi
    import spidev
    # bluetooth
    import bluetooth
    # camera
    # from picamera import PiCamera
    # bme280
    from adafruit_bme280 import basic as adafruit_bme280
    # GPIO
    import RPi.GPIO as GPIO
    # raspberry pi
    import digitalio
    import board
    # lidar
    import pigpio
except BaseException:
    IS_SIM = True
    print("exec as sim")
