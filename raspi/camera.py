# class LibcsCamera:
#     def __init__(self):
#         self.camera = PiCamera()
#         self.camera.resolution = (1920, 1080)
#         self.camera.hflip = True
#         self.camera.vflip = True

#     async def capture(self, filename):
#         await asyncio.sleep(1)
#         self.camera.capture(filename)

#     def record_start(self, filename):
#         self.camera.start_recording(filename)

#     def record_stop(self):
#         self.camera.stop_recording()

class LibcsRecord:
    def __init__(self, LOG_VIDEO_PATH):
        self.is_taking = False
        self.path = LOG_VIDEO_PATH
        self.record_start_time = time.time()

    def start(self, fps=10, count=1):
        self.cap = cv2.VideoCapture(0)
        self.w = int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH))  # カメラの幅を取得
        self.h = int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))  # カメラの高さを取得
        self.fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')  # 動画保存時の形式を設定
        self.fps = fps
        self.record_start_time = time.time()
        self.record_start_time_str = f"{datetime.now():%m%d_%H%M%S}"
        name = os.path.join(self.path, self.record_start_time_str + ".mp4")
        if count >= 2:
            name = os.path.join(self.path, self.record_start_time_str + "_" + str(count) + ".mp4")
        self.is_taking = True
        logger.info("log is saved in " + name)
        self.video = cv2.VideoWriter(name, self.fourcc, self.fps, (self.w, self.h))  # (保存名前、fourcc,fps,サイズ)
        logger.info("recording start")

    def stop(self):
        logger.info("recording stop")
        self.is_taking = False
        self.video.release()
        self.cap.release()
        cv2.destroyAllWindows()

    def update(self, pixhawk=None):
        self.read_start_time = time.time()
        ret, frame = self.cap.read()

        if pixhawk is not None:
            pixhawk.detection.update(
                frame,
                pixhawk.latitude_deg,
                pixhawk.longitude_deg,
                pixhawk.pitch_deg,
                pixhawk.roll_deg,
                pixhawk.yaw_deg,
                pixhawk.lidar)

        self.video.write(frame)  # 1フレーム保存する

    async def wait(self):
        elapsed_secs = time.time() - self.read_start_time
        if (1 / self.fps < elapsed_secs):
            await asyncio.sleep(0.0001)
        else:
            await asyncio.sleep(1 / self.fps - elapsed_secs)
