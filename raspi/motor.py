class LibcsMotor:
    def __init__(self, motor_output_pin_1, motor_output_pin_2=None):
        self.motor_output_pin_1 = motor_output_pin_1
        self.motor_output_pin_2 = motor_output_pin_2

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.motor_output_pin_1, GPIO.OUT)
        if self.motor_output_pin_2 is not None:
            GPIO.setup(self.motor_output_pin_2, GPIO.OUT)

    async def rotate(self, rotate_time=1, reverse=False):
        if reverse and self.motor_output_pin_2 is not None:
            GPIO.output(self.motor_output_pin_2, 1)
            await asyncio.sleep(rotate_time)
            GPIO.output(self.motor_output_pin_2, 0)
        else:
            GPIO.output(self.motor_output_pin_1, 1)
            await asyncio.sleep(rotate_time)
            GPIO.output(self.motor_output_pin_1, 0)

    def abort(self):
        GPIO.setup(self.motor_output_pin_1, GPIO.OUT)
        GPIO.output(self.motor_output_pin_1, 0)
        if self.motor_output_pin_2 is not None:
            GPIO.setup(self.motor_output_pin_2, GPIO.OUT)
            GPIO.output(self.motor_output_pin_2, 0)
