class LibcsAir:
    def __init__(self, board_pin, disp_v=1, disp_w=10):
        self.is_first = True
        self.board_pin = board_pin
        self.disp_v = disp_v
        self.disp_w = disp_w

        self.temprature = 0
        self.temprature_cov = 0
        self.relative_humidity = 0
        self.relative_humidity_cov = 0
        self.pressure = 0
        self.pressure_cov = 0
        self.altitude = 0
        self.altitude_cov = 0

        self.altitude_pre = 0
        self.altitude_pre_time = 0
        self.altvel = 0

        self.spi = board.SPI()
        bme_cs = digitalio.DigitalInOut(self.board_pin)
        self.bme280 = adafruit_bme280.Adafruit_BME280_SPI(self.spi, bme_cs)
        self.bme280.sea_level_pressure = SEA_LEVEL_PRESSURE

        self._temprature = self.bme280.temperature
        self._relative_humidity = self.bme280.relative_humidity
        self._pressure = self.bme280.pressure

        self._altitude = self.bme280.altitude
        self.altitude_pre = copy.deepcopy(self._altitude)
        self.altitude_pre_time = time.time()
        self.altvel = 0

    def update(self):
        self._temprature = self.bme280.temperature
        self._relative_humidity = self.bme280.relative_humidity
        self._pressure = self.bme280.pressure
        self._altitude = self.bme280.altitude

        if self.is_first:
            self.temprature = self.bme280.temperature
            self.relative_humidity = self.bme280.relative_humidity
            self.pressure = self.bme280.pressure
            self.altitude = self.bme280.altitude
            self.is_first = False
        else:
            self.temprature, self.temprature_cov = kalmanfilter(
                self._temprature, self.temprature, self.temprature_cov, self.disp_v, self.disp_w)
            self.relative_humidity, self.relative_humidity_cov = kalmanfilter(
                self._relative_humidity, self.relative_humidity, self.relative_humidity_cov, self.disp_v, self.disp_w)
            self.pressure, self.pressure_cov = kalmanfilter(
                self._pressure, self.pressure, self.pressure_cov, self.disp_v, self.disp_w)
            self.altitude, self.altitude_cov = kalmanfilter(
                self._altitude, self.altitude, self.altitude_cov, self.disp_v, self.disp_w)

        cur_time = time.time()
        self.altvel = (self.altitude - self.altitude_pre) / (cur_time - self.altitude_pre_time)
        self.altitude_pre = copy.deepcopy(self.altitude)
        self.altitude_pre_time = cur_time
