class LibcsBluetooth:  # Bluetooth
    def __init__(self, addr, port=1):
        self.__addr = addr
        self.__port = port
        self.__sock_server = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        self.data = []
        self.txt = ""

    def is_connected(self, addr: str):
        return True \
            if addr in subprocess.getoutput("hcitool con").split() \
            else False

    def server_reconnect(self):
        self.__sock_server = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

    async def server_connect(self):
        self.__sock_server = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        while True:
            try:
                self.__sock_server.connect((self.__addr, self.__port))
                await asyncio.sleep(2)
                break
            except bluetooth.BluetoothError:
                self.server_reconnect()
                await asyncio.sleep(1)
            except KeyboardInterrupt:
                break

    def client_connect(self):
        self.__sock_server = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        self.__sock_server.bind(("", self.__port))
        self.__sock_server.listen(1)
        self.__sock_client, self.__addr = self.__sock_server.accept()

    def server_disconnect(self):
        self.__sock_server.close()

    def client_disconnect(self):
        self.__sock_client.close()

    async def send(self, string: str, timeout: int = 0.1):
        try:
            self.__sock_server.send(string)
            await asyncio.sleep(timeout)
        except KeyboardInterrupt:
            self.server_disconnect()

    async def recv_int(self):
        try:
            data = self.__sock_client.recv(1024).decode()
            for _data in data:
                if len(self.data) < 15:
                    self.data.append(int(_data))
                else:
                    self.data.pop(0)
                    self.data.append(int(_data))
        except KeyboardInterrupt:
            self.server_disconnect()
            self.client_disconnect()

    async def recv_txt(self):
        try:
            self.txt = self.__sock_client.recv(1024).decode()
        except KeyboardInterrupt:
            self.server_disconnect()
            self.client_disconnect()

    def abort(self):
        self.server_disconnect()
        self.client_disconnect()
