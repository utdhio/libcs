class LibcsCds:
    def __init__(self, LIGHT_THH=LIGHT_THH_DEFAULT, disp_v=1, disp_w=10):
        self.is_first = True
        self.spi = spidev.SpiDev()  # インスタンスを生成
        self.LIGHT_THH = LIGHT_THH

        self.is_ok = False

        self._val = 0
        self.val = 0
        self.cov = 0
        self.disp_v = disp_v
        self.disp_w = disp_w

    def init(self, pin):
        self.spi.open(0, pin)  # ピンを指定
        self.spi.max_speed_hz = 1000000  # 転送速度

    # 光センサから値を取り出す
    def update(self):
        resp = self.spi.xfer2([0x68, 0x00])  # SPI通信で値を読み込む
        self._val = ((resp[0] << 8) + resp[1]) & 0x3FF  # 読み込んだ値を10ビットの数値に変換

        if self.is_first:
            self.val = self._val
            self.is_first = False
        else:
            self.val, self.cov = kalmanfilter(self._val, self.val, self.cov, self.disp_v, self.disp_w)
            if self.val > self.LIGHT_THH:
                self.is_ok = True

    # 光センサを終える
    def abort(self):
        self.spi.close()  # SPI通信を終了
