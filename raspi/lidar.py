class LibcsLidar:
    def __init__(self, disp_v=1, disp_w=5):
        self.is_first = True
        self.pi = pigpio.pi()  # どうやらこのpigpioとかいうライブラリを用いるらしい
        self._distance = 0  # 距離
        self.strength = 0  # 信号の強さらしいけどよくわからん

        self.distance = 0
        self.cov = 0
        self.disp_v = disp_v
        self.disp_w = disp_w

    def init(self):
        self.pi.set_mode(LIDAR_RX, pigpio.INPUT)  # ピンのセット
        self.pi.bb_serial_read_open(LIDAR_RX, LIDAR_BAUD_RATE)

    def update(self):
        '''
        * bb_serial_read: https://abyz.me.uk/rpi/pigpio/python.html#pigpio.pi

        * count: the number of bytes read
        * recv: data
          * chrome-extension://efaidnbmnnnibpcajpcglclefindmkaj/viewer.html?pdfurl=https%3A%2F%2Fwww.elecrow.com%2Fdownload%2FTF-MINI-LIDAR-USER-MANUAL.pdf&chunk=true
            * p.6-7あたりが参考になる
            * 0, 1: 0x59という値が入っている
            * 2: distanceのlower 8 bits
            * 3: distanceのhigher 8 bits
            * 4: strengthのlower 8 bits
            * 5: strengthのhigher 8 bits
            * 6: distance mode
              * short distance: 02
              * long distance: 07
              * 自動でやってくれる
              * versionによっては数字違うかも
            * 7: 予備のbytes. デフォルトでは00
            * 8: checksumのlower8bits
        * checksum: 誤り検出符号。渡される数字のすべての合計が正しいかチェック。
        '''
        self.count, self.recv = self.pi.bb_serial_read(LIDAR_RX)
        if self.count > 8:
            for i in range(0, self.count - 9, 9):  # なんで-9なのかわからん
                if self.recv[i] == 89 and self.recv[i + 1] == 89:  # 0x59 is 89
                    checksum = 0
                    for j in range(0, 8):
                        checksum += self.recv[i + j]
                    checksum %= 256  # lower 8bitsをとる
                    if checksum == self.recv[i + 8]:  # checksum確認
                        self._distance = self.recv[i + 2] + self.recv[i + 3] * 256
                        self.strength = self.recv[i + 4] + self.recv[i + 5] * 256

                        if self.is_first:
                            self.distance = self._distance
                            self.is_first = False
                        else:
                            self.distance, self.cov = kalmanfilter(
                                self._distance, self.distance, self.cov, self.disp_v, self.disp_w)

    # lidarを終える
    def abort(self):
        self.pi.bb_serial_read_close(LIDAR_RX)
        self.pi.stop()
