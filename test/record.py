import cv2
from datetime import datetime
import os
import time
import argparse


def task_record(seconds=10):
    fps = 20
    cap = cv2.VideoCapture(0)
    w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))  # カメラの幅を取得
    h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))  # カメラの高さを取得
    fourcc = cv2.VideoWriter_fourcc("m", "p", "4", "v")  # 動画保存時の形式を設定
    name = os.path.join("/home/dhio/videos/1011", f"{datetime.now():%m%d_%H%M%S}.mp4")
    print("log is saved in " + name)
    video = cv2.VideoWriter(name, fourcc, fps, (w, h))  # (保存名前、fourcc,fps,サイズ)
    print("recording start")
    for i in range(fps * seconds):
        start = time.time()
        ret, frame = cap.read()  # 1フレーム読み込み
        video.write(frame)  # 1フレーム保存する
        elapsed_secs = time.time() - start
        if(1 / fps < elapsed_secs):
            continue
        else:
            time.sleep(1 / fps - elapsed_secs)
    cap.release()
    video.release()
    cv2.destroyAllWindows()
    print("recording stop")


parser = argparse.ArgumentParser()
parser.add_argument("seconds", type=int)
args = parser.parse_args()

time.sleep(5)
task_record(args.seconds)
