import time
import argparse
import RPi.GPIO as GPIO

left_pin = 24
right_pin = 25
GPIO.setmode(GPIO.BCM)
GPIO.setup(left_pin, GPIO.OUT)
GPIO.setup(right_pin, GPIO.OUT)

def rotate_left(rotate_time=1):
    GPIO.output(left_pin, 1)
    time.sleep(rotate_time)
    GPIO.output(left_pin, 0)

def rotate_right(rotate_time=1):
    GPIO.output(right_pin, 1)
    time.sleep(rotate_time)
    GPIO.output(right_pin, 0)

parser = argparse.ArgumentParser(description='explanation')    # 2. パーサを作る

parser.add_argument('--time', "-t", type=int, default=1)
parser.add_argument('--rev', action='store_true')

args = parser.parse_args()    # 4. 引数を解析

if args.rev:
    rotate_right(args.time)
else:
    rotate_left(args.time)
