import RPi.GPIO as GPIO
import sys
import time
import serial
import struct

ResetPin = 12
device = "/dev/serial0"  # ttyS0
baudrate = 19200

def printable(l):
    x = struct.unpack(str(len(l)) + 'b', l)
    y = ''
    for i in range(len(x)):
        if x[i] >= 0:
            y = y + chr(x[i])
    return y

def sendcmd(cmd):
    print("send", cmd)
    lr.write(cmd)
    t = time.time()
    while (True):
        if (time.time() - t) > 5:
            print('panic: %s' % cmd)
            exit()
        line = lr.readline()
        if 'OK' in printable(line):
            print(line.decode())
            return True
        elif 'NG' in printable(line):
            print(line.decode())
            return False

class LoRa():
    def __init__(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setwarnings(False)
        GPIO.setup(ResetPin, GPIO.OUT)
        GPIO.output(ResetPin, 1)

        self.s = serial.Serial(device, baudrate)

    def reset(self):
        GPIO.output(ResetPin, 0)
        time.sleep(0.1)
        GPIO.output(ResetPin, 1)

    def open(self):
        self.s.open()

    def close(self):
        self.s.close()

    def readline(self, timeout=None):
        if timeout is not None:
            self.close()
            self.s.timeout = timeout
            self.open()
        line = self.s.readline()
        if timeout is not None:
            self.close()
            self.s.timeout = None
            self.open()
        return line

    def write(self, msg):
        msg += '\r\n'
        self.s.write(msg.encode('utf-8'))


lr = LoRa()


def send():
    count = 0
    while True:
        sendcmd(str(count))
        count += 1
        time.sleep(1)


def recv():
    while True:
        line = lr.readline()
        print(line)


if __name__ == "__main__":
    args = sys.argv
    if args[1] == "s":
        send()
    elif args[1] == "r":
        recv()
    else:
        pass
