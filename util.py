SEA_LEVEL_PRESSURE = 1013.25
# SYSTEM_ADDRESS_DEFAULT = "serial:///dev/serial0:921600" # raspbian
SYSTEM_ADDRESS_DEFAULT = "serial:///dev/ttyACM0:115200"
SYSTEM_ADDRESS_SIM = "udp://:14540"
SENSOR_CYCLE = 0.1  # s
PIXHAWK_CYCLE = 0.1  # s
PIXHAWK_SHOW_CYCLE = 0.25  # s

# THH: THresHold（閾値）
# 光センサ
LIGHT_THH_DEFAULT = 100  # 放出されたかどうかの閾値

# bluetooth
# $ hciconfig / $ ifconfig で分かる
MAC_ADDR_LIST = [
    "B8:27:EB:BD:90:B0",  # drone
    "B8:27:EB:DE:66:25",  # case
    "B8:27:EB:B9:5F:0E",  # spare
    "DC:A6:32:9A:50:0C",  # raspberry pi 4(AA:AA...になるかも)
    "B8:27:EB:56:93:D9",  # 瀬戸の手持ち
]
DRONE_ADDR = MAC_ADDR_LIST[3]
CASE_ADDR = MAC_ADDR_LIST[4]

# camera
ALPHA = np.deg2rad(24.4)  # 画角2*alpha
BETA = np.deg2rad(31.1)  # 画角2*beta

LAT_1deg = 110360
LNG_1deg = 90187


def kalmanfilter(cur, est, cov, disp_v=1, disp_w=5):
    cov += disp_v
    kalman_gain = cov / (cov + disp_w)
    est += kalman_gain * (cur - est)
    cov *= (1 - kalman_gain)
    return est, cov


def str_f(_float):
    return '{:.03f}'.format(_float)


def str_gps(_float):
    return '{:.07f}'.format(_float)


def cycle(_funcs, _args=None):
    global main_coroutines
    if isinstance(_funcs, list):
        for _func in _funcs:
            main_coroutines.append(_func())
    else:
        main_coroutines.append(_funcs())

    def _cycle(func):
        async def wrapper(*args, **kwargs):
            await func(*args, **kwargs)
        return wrapper
    return _cycle


def timeout(seconds):
    def _timeout(func):
        async def wrapper(*args, **kwargs):
            try:
                await asyncio.wait_for(func(*args, **kwargs), timeout=seconds)
            except asyncio.TimeoutError:
                print(f'{func.__name__} timeout')
        return wrapper
    return _timeout


async def wait_forever():
    while True:
        await asyncio.sleep(100)


def gps_distance(lat1, lng1, lat2, lng2):
    lat_diff = (lat1 - lat2) * LAT_1deg
    lng_diff = (lng1 - lng2) * LNG_1deg
    dist = np.sqrt(lat_diff**2 + lng_diff**2)
    return dist


def gps_dest(lat, lng, deg, dist):
    deg_rad = np.radians(deg)
    lat_dest = lat + (dist * np.cos(deg_rad)) / LAT_1deg
    lng_dest = lng + (dist * np.sin(deg_rad)) / LNG_1deg
    return lat_dest, lng_dest


def gps_dest_ne(lat, lng, north, east):
    return lat + north / LAT_1deg, lng + east / LNG_1deg


def letterbox(im, new_shape=(640, 640), color=(114, 114, 114), auto=True, scaleFill=False, scaleup=True, stride=32):
    # Resize and pad image while meeting stride-multiple constraints
    shape = im.shape[:2]  # current shape [height, width]
    if isinstance(new_shape, int):
        new_shape = (new_shape, new_shape)

    # Scale ratio (new / old)
    r = min(new_shape[0] / shape[0], new_shape[1] / shape[1])
    if not scaleup:  # only scale down, do not scale up (for better val mAP)
        r = min(r, 1.0)

    # Compute padding
    ratio = r, r  # width, height ratios
    new_unpad = int(round(shape[1] * r)), int(round(shape[0] * r))
    dw, dh = new_shape[1] - new_unpad[0], new_shape[0] - new_unpad[1]  # wh padding
    if auto:  # minimum rectangle
        dw, dh = np.mod(dw, stride), np.mod(dh, stride)  # wh padding
    elif scaleFill:  # stretch
        dw, dh = 0.0, 0.0
        new_unpad = (new_shape[1], new_shape[0])
        ratio = new_shape[1] / shape[1], new_shape[0] / shape[0]  # width, height ratios

    dw /= 2  # divide padding into 2 sides
    dh /= 2

    if shape[::-1] != new_unpad:  # resize
        im = cv2.resize(im, new_unpad, interpolation=cv2.INTER_LINEAR)
    top, bottom = int(round(dh - 0.1)), int(round(dh + 0.1))
    left, right = int(round(dw - 0.1)), int(round(dw + 0.1))
    im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)  # add border
    return im, ratio, (dw, dh)


def non_max_suppression(prediction, conf_thres=0.25, iou_thres=0.45, classes=None, agnostic=False, multi_label=False,
                        labels=(), max_det=300):
    """Runs Non-Maximum Suppression (NMS) on inference results

    Returns:
         list of detections, on (n,6) tensor per image [xyxy, conf, cls]
    """
    def xywh2xyxy(x):
        # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
        y = x.clone() if isinstance(x, torch.Tensor) else np.copy(x)
        y[:, 0] = x[:, 0] - x[:, 2] / 2  # top left x
        y[:, 1] = x[:, 1] - x[:, 3] / 2  # top left y
        y[:, 2] = x[:, 0] + x[:, 2] / 2  # bottom right x
        y[:, 3] = x[:, 1] + x[:, 3] / 2  # bottom right y
        return y

    nc = prediction.shape[2] - 5  # number of classes
    xc = prediction[..., 4] > conf_thres  # candidates

    # Checks
    assert 0 <= conf_thres <= 1, f'Invalid Confidence threshold {conf_thres}, valid values are between 0.0 and 1.0'
    assert 0 <= iou_thres <= 1, f'Invalid IoU {iou_thres}, valid values are between 0.0 and 1.0'

    # Settings
    min_wh, max_wh = 2, 4096  # (pixels) minimum and maximum box width and height
    max_nms = 30000  # maximum number of boxes into torchvision.ops.nms()
    time_limit = 10.0  # seconds to quit after
    redundant = True  # require redundant detections
    multi_label &= nc > 1  # multiple labels per box (adds 0.5ms/img)
    merge = False  # use merge-NMS

    t = time.time()
    output = [torch.zeros((0, 6), device=prediction.device)] * prediction.shape[0]
    for xi, x in enumerate(prediction):  # image index, image inference
        # Apply constraints
        # x[((x[..., 2:4] < min_wh) | (x[..., 2:4] > max_wh)).any(1), 4] = 0  # width-height
        x = x[xc[xi]]  # confidence

        # Cat apriori labels if autolabelling
        if labels and len(labels[xi]):
            l = labels[xi]
            v = torch.zeros((len(l), nc + 5), device=x.device)
            v[:, :4] = l[:, 1:5]  # box
            v[:, 4] = 1.0  # conf
            v[range(len(l)), l[:, 0].long() + 5] = 1.0  # cls
            x = torch.cat((x, v), 0)

        # If none remain process next image
        if not x.shape[0]:
            continue

        # Compute conf
        x[:, 5:] *= x[:, 4:5]  # conf = obj_conf * cls_conf

        # Box (center x, center y, width, height) to (x1, y1, x2, y2)
        box = xywh2xyxy(x[:, :4])

        # Detections matrix nx6 (xyxy, conf, cls)
        if multi_label:
            i, j = (x[:, 5:] > conf_thres).nonzero(as_tuple=False).T
            x = torch.cat((box[i], x[i, j + 5, None], j[:, None].float()), 1)
        else:  # best class only
            conf, j = x[:, 5:].max(1, keepdim=True)
            x = torch.cat((box, conf, j.float()), 1)[conf.view(-1) > conf_thres]

        # Filter by class
        if classes is not None:
            x = x[(x[:, 5:6] == torch.tensor(classes, device=x.device)).any(1)]

        # Apply finite constraint
        # if not torch.isfinite(x).all():
        #     x = x[torch.isfinite(x).all(1)]

        # Check shape
        n = x.shape[0]  # number of boxes
        if not n:  # no boxes
            continue
        elif n > max_nms:  # excess boxes
            x = x[x[:, 4].argsort(descending=True)[:max_nms]]  # sort by confidence

        # Batched NMS
        c = x[:, 5:6] * (0 if agnostic else max_wh)  # classes
        boxes, scores = x[:, :4] + c, x[:, 4]  # boxes (offset by class), scores
        i = torchvision.ops.nms(boxes, scores, iou_thres)  # NMS
        if i.shape[0] > max_det:  # limit detections
            i = i[:max_det]
        if merge and (1 < n < 3E3):  # Merge NMS (boxes merged using weighted mean)
            # update boxes as boxes(i,4) = weights(i,n) * boxes(n,4)
            iou = box_iou(boxes[i], boxes) > iou_thres  # iou matrix
            weights = iou * scores[None]  # box weights
            x[i, :4] = torch.mm(weights, x[:, :4]).float() / weights.sum(1, keepdim=True)  # merged boxes
            if redundant:
                i = i[iou.sum(1) > 1]  # require redundancy

        output[xi] = x[i]
        if (time.time() - t) > time_limit:
            print(f'WARNING: NMS time limit {time_limit}s exceeded')
            break  # time limit exceeded

    return output


def parse_pred(detection, pred):
    if len(pred[0]) > 0:
        if len(pred[0][0]) == 6:
            s = 1 - (pred[0][0][0] + pred[0][0][2]) / 320  # -1~1
            t = 1 - (pred[0][0][1] + pred[0][0][3]) / 320  # -1~1
            omega = np.matrix([[np.cos(detection.yaw), np.sin(detection.yaw)],
                               [-np.sin(detection.yaw), np.cos(detection.yaw)]])
            origin = np.matrix([[np.tan(detection.roll + np.arctan(s * np.tan(BETA)))],
                                [np.tan(detection.pitch + np.arctan(t * np.tan(ALPHA)))]])
            xy = omega * origin * detection.z
            # return float(-xy[1]), float(xy[0]), float(pred[0][0][4])
            return float(xy[1]*1.2), float(-xy[0]*1.2), float(pred[0][0][4])
    else:
        return None, None, None
